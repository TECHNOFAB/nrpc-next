package main

import (
	"context"
	"errors"
	"fmt"
	"github.com/nats-io/nats.go"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/exporters/stdout/stdouttrace"
	"go.opentelemetry.io/otel/sdk/resource"
	"go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.10.0"
	oteltrace "go.opentelemetry.io/otel/trace"
	"io"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"time"

	// This is the package containing the generated *.pb.go and *.nrpc.go
	// files.
	"gitlab.com/TECHNOFAB/nrpc-next/examples/tracing_helloworld/helloworld"
)

var tracer oteltrace.Tracer

// server implements the helloworld.GreeterServer interface.
type server struct{}

// SayHello is an implementation of the SayHello method from the definition of
// the Greeter service.
func (s *server) SayHello(ctx context.Context, req helloworld.HelloRequest) (resp helloworld.HelloReply, err error) {
	var span oteltrace.Span
	ctx, span = tracer.Start(ctx, "nrpc.SayHello", oteltrace.WithSpanKind(oteltrace.SpanKindServer))
	defer span.End()

	resp.Message = "Hello " + req.Name
	if rand.Intn(10) < 4 { // will fail 40% of the time
		err = errors.New("random failure simulated on the server")
	}
	//time.Sleep(time.Duration(rand.Intn(100)) * time.Millisecond) // random delay
	return
}

// newExporter returns a console exporter.
func newExporter(w io.Writer) (trace.SpanExporter, error) {
	return stdouttrace.New(
		stdouttrace.WithWriter(w),
		// Use human-readable output.
		stdouttrace.WithPrettyPrint(),
		// Do not print timestamps for the demo.
		stdouttrace.WithoutTimestamps(),
	)
}

// newResource returns a resource describing this application.
func newResource() *resource.Resource {
	r, _ := resource.Merge(
		resource.Default(),
		resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String("tracing_greeter_server"),
			semconv.ServiceVersionKey.String("v0.1.0"),
			attribute.String("environment", "example"),
		),
	)
	return r
}

func initTracer() func(context.Context) {
	f, err := os.Create("traces_server.txt")
	if err != nil {
		log.Fatal(err)
	}

	exporter, err := newExporter(f)
	if err != nil {
		log.Fatal(err)
	}
	tp := trace.NewTracerProvider(
		trace.WithBatcher(exporter),
		trace.WithResource(newResource()),
	)
	otel.SetTracerProvider(tp)

	return func(ctx context.Context) {
		defer f.Close()
		err := tp.Shutdown(ctx)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func initJaegerTracer(url string) (*trace.TracerProvider, error) {
	// Create the Jaeger exporter
	exp, err := jaeger.New(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint(url)))
	if err != nil {
		return nil, err
	}
	tp := trace.NewTracerProvider(
		// Always be sure to batch in production.
		trace.WithBatcher(exp),
		trace.WithResource(newResource()),
	)
	otel.SetTracerProvider(tp)
	return tp, nil
}

func init() {
	tracer = otel.Tracer("gitlab.com/technobot-dev/utils/nrpc/examples/tracing_helloworld/server")
}

func main() {
	ctx := context.TODO()
	t, err := initJaegerTracer("http://localhost:14268/api/traces")
	if err != nil {
		log.Fatal(err)
	}
	defer t.Shutdown(ctx)

	// Connect to the NATS server.
	nc, err := nats.Connect(nats.DefaultURL, nats.Timeout(5*time.Second))
	if err != nil {
		log.Fatal(err)
	}
	defer nc.Close()

	// Our server implementation.
	s := &server{}
	rand.Seed(time.Now().UnixNano())

	// The NATS handler from the helloworld.proto file.
	h := helloworld.NewGreeterHandler(ctx, nc, s)

	// Start a NATS subscription using the handler. You can also use the
	// QueueSubscribe() method for a load-balanced set of servers.
	// use this or the shorthand below
	// sub, err := nc.Subscribe(h.Subject(), h.Handler)
	sub, err := h.Subscribe()
	if err != nil {
		log.Fatal(err)
	}
	defer sub.Unsubscribe()

	// Keep running until ^C.
	fmt.Println("server is running, ^C quits.")
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	close(c)
}
