package main

import (
	"context"
	"fmt"
	"github.com/nats-io/nats.go"
	"gitlab.com/TECHNOFAB/nrpc-next/examples/tracing_helloworld/helloworld"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/exporters/stdout/stdouttrace"
	"go.opentelemetry.io/otel/sdk/resource"
	"go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.10.0"
	oteltrace "go.opentelemetry.io/otel/trace"
	"io"
	"log"
	"os"
	"time"
)

var tracer oteltrace.Tracer

// newExporter returns a console exporter.
func newExporter(w io.Writer) (trace.SpanExporter, error) {
	return stdouttrace.New(
		stdouttrace.WithWriter(w),
		// Use human-readable output.
		stdouttrace.WithPrettyPrint(),
		// Do not print timestamps for the demo.
		stdouttrace.WithoutTimestamps(),
	)
}

// newResource returns a resource describing this application.
func newResource() *resource.Resource {
	r, _ := resource.Merge(
		resource.Default(),
		resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String("tracing_greeter_client"),
			semconv.ServiceVersionKey.String("v0.1.0"),
			attribute.String("environment", "example"),
		),
	)
	return r
}

func initFileTracer() func(context.Context) {
	f, err := os.Create("traces.txt")
	if err != nil {
		log.Fatal(err)
	}

	exporter, err := newExporter(f)
	if err != nil {
		log.Fatal(err)
	}
	tp := trace.NewTracerProvider(
		trace.WithBatcher(exporter),
		trace.WithResource(newResource()),
	)
	otel.SetTracerProvider(tp)

	return func(ctx context.Context) {
		defer f.Close()
		err := tp.Shutdown(ctx)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func initJaegerTracer(url string) (*trace.TracerProvider, error) {
	// Create the Jaeger exporter
	exp, err := jaeger.New(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint(url)))
	if err != nil {
		return nil, err
	}
	tp := trace.NewTracerProvider(
		// Always be sure to batch in production.
		trace.WithBatcher(exp),
		trace.WithResource(newResource()),
	)
	otel.SetTracerProvider(tp)
	return tp, nil
}

func doWork(ctx context.Context) {
	var span oteltrace.Span
	ctx, span = tracer.Start(ctx, "doWork")
	defer span.End()

	log.Printf("traceid: %v, spanid: %v", span.SpanContext().TraceID(), span.SpanContext().SpanID())

	// Connect to the NATS server.

	_, connect_span := tracer.Start(ctx, "nats.Connect")
	nc, err := nats.Connect(nats.DefaultURL, nats.Timeout(5*time.Second))
	if err != nil {
		log.Panic(err)
	}
	defer nc.Close()
	connect_span.End()

	// This is our generated client.
	cli := helloworld.NewGreeterClient(nc)

	// Contact the server and print out its response.
	span.AddEvent("sending HelloRequest")
	resp, err := cli.SayHello(ctx, helloworld.HelloRequest{Name: "world"})
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "error")

		log.Panic(err)
	}

	// print
	span.AddEvent("printing response message")
	fmt.Printf("Greeting: %s\n", resp.Message)
}

func init() {
	tracer = otel.Tracer("gitlab.com/technobot-dev/utils/nrpc/examples/tracing_helloworld/client")
}

func main() {
	ctx := context.TODO()
	t, err := initJaegerTracer("http://localhost:14268/api/traces")
	if err != nil {
		log.Panic(err)
	}
	defer func() {
		log.Println("shutting down tracer...")
		err := t.Shutdown(ctx)
		if err != nil {
			log.Printf("error shutting down tracer: %v", err)
		}
	}()
	//shutdown := initFileTracer()
	//defer shutdown(ctx)

	doWork(ctx)
}
