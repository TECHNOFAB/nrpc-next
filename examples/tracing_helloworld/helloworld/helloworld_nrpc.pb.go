// Package helloworld
// This code was autogenerated by protoc-gen-nrpc-next from helloworld.proto, do not edit.
// flags: Prometheus=false Tracing=true
package helloworld

import (
	"context"
	"github.com/nats-io/nats.go"
	nrpc_next "gitlab.com/TECHNOFAB/nrpc-next/nrpc.go"
	"google.golang.org/protobuf/proto"
	"log"
	"time"
	// OpenTelemetry
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"
)

var helloworldTracer trace.Tracer

// GreeterServer is the interface that providers of the service
// Greeter should implement.
type GreeterServer interface {
	SayHello(ctx context.Context, req HelloRequest) (resp HelloReply, err error)
}

// GreeterHandler provides a NATS subscription handler that can serve a
// subscription using a given GreeterServer implementation.
type GreeterHandler struct {
	ctx       context.Context
	workers   *nrpc_next.WorkerPool
	nc        nrpc_next.NatsConn
	server    GreeterServer
	encodings []string
}

func NewGreeterHandler(ctx context.Context, nc nrpc_next.NatsConn, s GreeterServer) *GreeterHandler {
	return &GreeterHandler{
		ctx:       ctx,
		nc:        nc,
		server:    s,
		encodings: []string{"protobuf"},
	}
}

func NewGreeterConcurrentHandler(workers *nrpc_next.WorkerPool, nc nrpc_next.NatsConn, s GreeterServer) *GreeterHandler {
	return &GreeterHandler{
		workers: workers,
		nc:      nc,
		server:  s,
	}
}

func (h *GreeterHandler) Subscribe() (*nats.Subscription, error) {
	return h.nc.Subscribe(h.Subject(), h.Handler)
}

// SetEncodings sets the output encodings when using a '*Publish' function
func (h *GreeterHandler) SetEncodings(encodings []string) {
	h.encodings = encodings
}

func (h *GreeterHandler) Subject() string {
	return "Greeter.>"
}
func (h *GreeterHandler) Handler(msg *nats.Msg) {
	var ctx context.Context
	if h.workers != nil {
		ctx = h.workers.Context
	} else {
		ctx = h.ctx
	}
	if msg.Header.Get("trace_id") != "" {
		traceId, err := trace.TraceIDFromHex(msg.Header.Get("trace_id"))
		if err != nil {
			log.Printf("failed to get traceid from nats msg: %v", err)
		} else {
			ctx = trace.ContextWithSpanContext(ctx, trace.NewSpanContext(trace.SpanContextConfig{
				TraceID: traceId,
			}))
		}

		var span trace.Span
		ctx, span = helloworldTracer.Start(ctx, "nrpc_next.GreeterHandler")
		defer span.End()
	}
	request := nrpc_next.NewRequest(ctx, h.nc, msg.Subject, msg.Reply)
	// extract method name & encoding from subject
	_, _, name, tail, err := nrpc_next.ParseSubject(
		"", 0, "Greeter", 0, msg.Subject)
	if err != nil {
		log.Printf("GreeterHandler: Greeter subject parsing failed: %v", err)
		return
	}
	request.MethodName = name
	request.SubjectTail = tail
	// call handler and form response
	var immediateError *nrpc_next.Error
	switch name {
	case "SayHello":
		_, request.Encoding, err = nrpc_next.ParseSubjectTail(0, request.SubjectTail)
		if err != nil {
			log.Printf("SayHelloHanlder: SayHello subject parsing failed: %v", err)
			break
		}
		var req HelloRequest
		if err := nrpc_next.Unmarshal(request.Encoding, msg.Data, &req); err != nil {
			log.Printf("SayHelloHandler: SayHello request unmarshal failed: %v", err)
			immediateError = &nrpc_next.Error{
				Type:    nrpc_next.Error_CLIENT,
				Message: "bad request received: " + err.Error(),
			}
		} else {
			request.Handler = func(ctx context.Context) (proto.Message, error) {
				innerResp, err := h.server.SayHello(ctx, req)
				if err != nil {
					return nil, err
				}
				return &innerResp, err
			}
		}
	default:
		log.Printf("GreeterHandler: unknown name %q", name)
		immediateError = &nrpc_next.Error{
			Type:    nrpc_next.Error_CLIENT,
			Message: "unknown name: " + name,
		}
	}
	if immediateError == nil {
		if h.workers != nil {
			// Try queuing the request
			if err := h.workers.QueueRequest(request); err != nil {
				log.Printf("nrpc_next: Error queuing the request: %s", err)
			}
		} else {
			// Run the handler synchronously
			request.RunAndReply()
		}
	}
	if immediateError != nil {
		if err := request.SendReply(nil, immediateError); err != nil {
			log.Printf("GreeterHandler: Greeter handler failed to publish the response: %s", err)
		}
	} else {
	}
}

type GreeterClient struct {
	nc       nrpc_next.NatsConn
	Subject  string
	Encoding string
	Timeout  time.Duration
}

func NewGreeterClient(nc nrpc_next.NatsConn) *GreeterClient {
	return &GreeterClient{
		nc:       nc,
		Subject:  "Greeter",
		Encoding: "protobuf",
		Timeout:  5 * time.Second,
	}
}
func (c *GreeterClient) SayHello(
	ctx context.Context, req HelloRequest,
) (
	resp HelloReply, err error,
) {
	var span trace.Span
	ctx, span = helloworldTracer.Start(ctx, "nrpc_next.SayHello")
	defer span.End()
	subject := c.Subject + "." + "SayHello"
	// call
	err = nrpc_next.Call(ctx, &req, &resp, c.nc, subject, c.Encoding, c.Timeout)
	if err != nil {
		return // already logged
	}
	return
}

type HelloworldClient struct {
	nc              nrpc_next.NatsConn
	defaultEncoding string
	defaultTimeout  time.Duration
	Greeter         *GreeterClient
}

func NewHelloworldClient(nc nrpc_next.NatsConn) *HelloworldClient {
	c := HelloworldClient{
		nc:              nc,
		defaultEncoding: "protobuf",
		defaultTimeout:  5 * time.Second,
	}
	c.Greeter = NewGreeterClient(nc)
	return &c
}

func (c *HelloworldClient) SetEncoding(encoding string) {
	c.defaultEncoding = encoding
	if c.Greeter != nil {
		c.Greeter.Encoding = encoding
	}
}

func (c *HelloworldClient) SetTimeout(t time.Duration) {
	c.defaultTimeout = t
	if c.Greeter != nil {
		c.Greeter.Timeout = t
	}
}

func init() {
	helloworldTracer = otel.Tracer("gitlab.com/TECHNOFAB/nrpc-next/nrpc.go")

}
