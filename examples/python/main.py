import asyncio
import nats

import helloworld_nrpc
import helloworld_pb2


class Server(helloworld_nrpc.IGreeterServer):
    async def SayHello(self, req):
        return helloworld_pb2.HelloReply(message="Hello" + req.name)


async def main():
    nc: nats.NATS = await nats.connect('nats://localhost:4222')

    h = helloworld_nrpc.GreeterHandler(nc, Server())
    sub = await h.subscribe()

    c = helloworld_nrpc.GreeterClient(nc)

    r = await c.SayHello(helloworld_pb2.HelloRequest(name="World"))
    print("Greeting:", r.message)

    await sub.unsubscribe()
    await nc.drain()


if __name__ == "__main__":
    asyncio.run(main())
