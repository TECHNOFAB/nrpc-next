package nrpc_next

import (
	"log"
	"os"
	"testing"
	"time"

	"github.com/nats-io/nats-server/v2/logger"
	"github.com/nats-io/nats-server/v2/server"
)

var NatsURL string

func TestMain(m *testing.M) {
	natsServer, _ := server.NewServer(&server.Options{Port: server.RANDOM_PORT})
	natsServer.SetLogger(
		logger.NewStdLogger(false, false, false, false, false),
		false, false)
	go natsServer.Start()
	defer natsServer.Shutdown()

	if !natsServer.ReadyForConnections(time.Second) {
		log.Fatal("Cannot start the NATS server")
	}
	NatsURL = "nats://" + natsServer.Addr().String()

	os.Exit(m.Run())
}
