package py_lang

import (
	_ "embed"
	"github.com/golang/protobuf/protoc-gen-go/descriptor"
	nrpc_next "gitlab.com/TECHNOFAB/nrpc-next/nrpc.go"
	"gitlab.com/TECHNOFAB/nrpc-next/protoc-gen-nrpc-next/types"
	"google.golang.org/protobuf/compiler/protogen"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/pluginpb"
	"log"
	"strings"
	"text/template"
)

var (
	//go:embed py.tmpl
	tFile string
)

func splitTypePath(name string) []string {
	if len(name) == 0 {
		log.Fatal("Empty message type")
	}
	if name[0] != '.' {
		log.Fatalf("Expect message type name to start with '.', but it is '%s'", name)
	}
	return strings.Split(name[1:], ".")
}

func lookupFileDescriptor(name string, request *pluginpb.CodeGeneratorRequest) *descriptor.FileDescriptorProto {
	for _, fd := range request.GetProtoFile() {
		if fd.GetPackage() == name {
			return fd
		}
	}
	return nil
}

func lookupMessageType(name string, request *pluginpb.CodeGeneratorRequest) (*descriptor.FileDescriptorProto, *descriptor.DescriptorProto) {
	path := splitTypePath(name)

	pkgpath := path[:len(path)-1]

	var fd *descriptor.FileDescriptorProto
	for {
		pkgname := strings.Join(pkgpath, ".")
		fd = lookupFileDescriptor(pkgname, request)
		if fd != nil {
			break
		}
		if len(pkgpath) == 1 {
			log.Fatalf("Could not find the .proto file for package '%s' (from message %s)", pkgname, name)
		}
		pkgpath = pkgpath[:len(pkgpath)-1]
	}

	path = path[len(pkgpath):]

	var d *descriptor.DescriptorProto
	for _, mt := range fd.GetMessageType() {
		if mt.GetName() == path[0] {
			d = mt
			break
		}
	}
	if d == nil {
		log.Fatalf("No such type '%s' in package '%s'", path[0], strings.Join(pkgpath, "."))
	}
	for i, token := range path[1:] {
		var found bool
		for _, nd := range d.GetNestedType() {
			if nd.GetName() == token {
				d = nd
				found = true
				break
			}
		}
		if !found {
			log.Fatalf("No such nested type '%s' in '%s.%s'",
				token, strings.Join(pkgpath, "."), strings.Join(path[:i+1], "."))
		}
	}
	return fd, d
}

func pkgSubject(fd *descriptor.FileDescriptorProto) string {
	if options := fd.GetOptions(); options != nil {
		e := proto.GetExtension(options, nrpc_next.E_PackageSubject)
		if value, ok := e.(string); ok && len(value) > 0 {
			return value
		}
	}
	return fd.GetPackage()
}

func modAlias(mod string) string {
	return strings.Replace(strings.Replace(mod, "_", "__", -1), ".", "_dot_", -1)
}

func fdMod(fd string) string {
	return strings.Replace(strings.TrimSuffix(fd, ".proto"), "/", ".", -1) + "_pb2"
}

func getPyType(pbType string, request *pluginpb.CodeGeneratorRequest) (string, string) {
	if !strings.Contains(pbType, ".") {
		return "", pbType
	}
	fd, _ := lookupMessageType(pbType, request)
	name := strings.TrimPrefix(pbType, "."+fd.GetPackage()+".")
	name = strings.Replace(name, ".", "_", -1)
	return fd.GetPackage(), name
}

func GenerateFile(gen *protogen.Plugin, file *protogen.File, opts *types.GeneratorFuncOpts) *protogen.GeneratedFile {
	filename := file.GeneratedFilenamePrefix + "_nrpc.py"
	g := gen.NewGeneratedFile(filename, file.GoImportPath)

	funcMap := template.FuncMap{
		"Prometheus": func() bool {
			return opts.Prometheus
		},
		"Tracing": func() bool {
			return opts.Tracing
		},
		"JoinStrings": strings.Join,
		"ConvertBoolToPython": func(b bool) string {
			if b {
				return "True"
			} else {
				return "False"
			}
		},
		"PyType": func(in string) string {
			mod, typ := getPyType(in, gen.Request)
			return modAlias(fdMod(mod)) + "." + typ
		},
		"ExtraImports": func() map[string]string {
			res := make(map[string]string)
			for _, service := range file.Proto.Service {
				for _, md := range service.Method {
					input := md.GetInputType()
					mod, _ := getPyType(input, gen.Request)
					res[fdMod(mod)] = modAlias(fdMod(mod))
					output := md.GetOutputType()
					mod, _ = getPyType(output, gen.Request)
					res[fdMod(mod)] = modAlias(fdMod(mod))
				}
			}

			return res
		},
		"GetPkgSubject": pkgSubject,
		"GetPkgSubjectParams": func(fd *descriptor.FileDescriptorProto) []string {
			if opts := fd.GetOptions(); opts != nil {
				e := proto.GetExtension(opts, nrpc_next.E_PackageSubjectParams)
				value := e.([]string)
				return value
			}
			return nil
		},
		"GetServiceSubject": func(sd *descriptor.ServiceDescriptorProto) string {
			if opts := sd.GetOptions(); opts != nil {
				s := proto.GetExtension(opts, nrpc_next.E_ServiceSubject)
				if value, ok := s.(string); ok && s != "" {
					return value
				}
			}
			if opts := file.Proto.GetOptions(); opts != nil {
				s := proto.GetExtension(opts, nrpc_next.E_ServiceSubjectRule)
				switch s.(nrpc_next.SubjectRule) {
				case nrpc_next.SubjectRule_COPY:
					return sd.GetName()
				case nrpc_next.SubjectRule_TOLOWER:
					return strings.ToLower(sd.GetName())
				}
			}
			return sd.GetName()
		},
		"GetServiceSubjectParams": func(sd *descriptor.ServiceDescriptorProto) []string {
			if opts := sd.GetOptions(); opts != nil {
				e := proto.GetExtension(opts, nrpc_next.E_ServiceSubjectParams)
				if s, ok := e.([]string); ok {
					return s
				}
			}
			return []string{}
		},
		"GetMethodSubject": func(md *descriptor.MethodDescriptorProto) string {
			if opts := md.GetOptions(); opts != nil {
				s := proto.GetExtension(opts, nrpc_next.E_MethodSubject)
				if value, ok := s.(string); ok && value != "" {
					return value
				}
			}
			if opts := file.Proto.GetOptions(); opts != nil {
				s := proto.GetExtension(opts, nrpc_next.E_MethodSubjectRule)
				switch s.(nrpc_next.SubjectRule) {
				case nrpc_next.SubjectRule_COPY:
					return md.GetName()
				case nrpc_next.SubjectRule_TOLOWER:
					return strings.ToLower(md.GetName())
				}
			}
			return md.GetName()
		},
		"GetMethodSubjectParams": func(md *descriptor.MethodDescriptorProto) []string {
			if opts := md.GetOptions(); opts != nil {
				e := proto.GetExtension(opts, nrpc_next.E_MethodSubjectParams)
				if s, ok := e.([]string); ok {
					return s
				}
			}
			return []string{}
		},
		"HasStreamedReply": func(md *descriptor.MethodDescriptorProto) bool {
			if opts := md.GetOptions(); opts != nil {
				e := proto.GetExtension(opts, nrpc_next.E_StreamedReply)
				if s, ok := e.(bool); ok {
					return s
				}
			}
			return false
		},
	}

	t := template.Must(template.New(".").Funcs(funcMap).Parse(tFile))
	err := t.Execute(g, file.Proto)
	if err != nil {
		panic(err)
	}

	return g
}
