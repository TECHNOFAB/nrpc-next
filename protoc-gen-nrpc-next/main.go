package main

import (
	"flag"
	"fmt"
	go_lang "gitlab.com/TECHNOFAB/nrpc-next/protoc-gen-nrpc-next/go-lang"
	py_lang "gitlab.com/TECHNOFAB/nrpc-next/protoc-gen-nrpc-next/py-lang"
	"gitlab.com/TECHNOFAB/nrpc-next/protoc-gen-nrpc-next/types"
	"google.golang.org/protobuf/compiler/protogen"
)

var supportedLangs = map[string]types.GeneratorFuncType{
	"go": go_lang.GenerateFile,
	"py": py_lang.GenerateFile,
}

func main() {
	var flags flag.FlagSet
	lang := flags.String("lang", "go", "language to generate the handlers in")
	prometheus := flags.Bool("prometheus", false, "enable prometheus metrics")
	tracing := flags.Bool("tracing", false, "enable tracing via opentracing")

	protogen.Options{
		ParamFunc: flags.Set,
	}.Run(func(gen *protogen.Plugin) error {
		if generatorFunc, ok := supportedLangs[*lang]; ok {
			for _, f := range gen.Files {
				if !f.Generate {
					continue
				}
				// skip generation if there are no services
				if len(f.Proto.Service) == 0 {
					continue
				}
				generatorFunc(gen, f, &types.GeneratorFuncOpts{
					Prometheus: *prometheus,
					Tracing:    *tracing,
				})
			}
			return nil
		}
		return fmt.Errorf("protoc-gen-nrpc-next does not support language %s", *lang)
	})
}
