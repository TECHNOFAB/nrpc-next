package go_lang

import (
	_ "embed"
	"fmt"
	"github.com/golang/protobuf/protoc-gen-go/descriptor"
	nrpc_next "gitlab.com/TECHNOFAB/nrpc-next/nrpc.go"
	"gitlab.com/TECHNOFAB/nrpc-next/protoc-gen-nrpc-next/types"
	"google.golang.org/protobuf/compiler/protogen"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/pluginpb"
	"log"
	"strings"
	"text/template"
)

var (
	//go:embed go.tmpl
	tFile string
)

// getGoPackage returns the file's go_package option.
// If it containts a semicolon, only the part before it is returned.
func getGoPackage(fd *descriptor.FileDescriptorProto) string {
	pkg := fd.GetOptions().GetGoPackage()
	if strings.Contains(pkg, ";") {
		parts := strings.Split(pkg, ";")
		if len(parts) > 2 {
			log.Fatalf(
				"go_package '%s' contains more than 1 ';'",
				pkg)
		}
		pkg = parts[1]
	}

	return pkg
}

func splitTypePath(name string) []string {
	if len(name) == 0 {
		log.Fatal("Empty message type")
	}
	if name[0] != '.' {
		log.Fatalf("Expect message type name to start with '.', but it is '%s'", name)
	}
	return strings.Split(name[1:], ".")
}

func lookupFileDescriptor(name string, request *pluginpb.CodeGeneratorRequest) *descriptor.FileDescriptorProto {
	for _, fd := range request.GetProtoFile() {
		if fd.GetPackage() == name {
			return fd
		}
	}
	return nil
}

func lookupMessageType(name string, request *pluginpb.CodeGeneratorRequest) (*descriptor.FileDescriptorProto, *descriptor.DescriptorProto) {
	path := splitTypePath(name)

	pkgpath := path[:len(path)-1]

	var fd *descriptor.FileDescriptorProto
	for {
		pkgname := strings.Join(pkgpath, ".")
		fd = lookupFileDescriptor(pkgname, request)
		if fd != nil {
			break
		}
		if len(pkgpath) == 1 {
			log.Fatalf("Could not find the .proto file for package '%s' (from message %s)", pkgname, name)
		}
		pkgpath = pkgpath[:len(pkgpath)-1]
	}

	path = path[len(pkgpath):]

	var d *descriptor.DescriptorProto
	for _, mt := range fd.GetMessageType() {
		if mt.GetName() == path[0] {
			d = mt
			break
		}
	}
	if d == nil {
		log.Fatalf("No such type '%s' in package '%s'", path[0], strings.Join(pkgpath, "."))
	}
	for i, token := range path[1:] {
		var found bool
		for _, nd := range d.GetNestedType() {
			if nd.GetName() == token {
				d = nd
				found = true
				break
			}
		}
		if !found {
			log.Fatalf("No such nested type '%s' in '%s.%s'",
				token, strings.Join(pkgpath, "."), strings.Join(path[:i+1], "."))
		}
	}
	return fd, d
}

func pkgSubject(fd *descriptor.FileDescriptorProto) string {
	if options := fd.GetOptions(); options != nil {
		e := proto.GetExtension(options, nrpc_next.E_PackageSubject)
		if value, ok := e.(string); ok {
			return value
		}
	}
	return fd.GetPackage()
}

func getGoType(pbType string, request *pluginpb.CodeGeneratorRequest) (string, string) {
	if !strings.Contains(pbType, ".") {
		return "", pbType
	}
	fd, _ := lookupMessageType(pbType, request)
	name := strings.TrimPrefix(pbType, "."+fd.GetPackage()+".")
	name = strings.Replace(name, ".", "_", -1)
	return getGoPackage(fd), name
}

func getPkgImportName(goPkg string, currentFile *descriptor.FileDescriptorProto) string {
	if goPkg == getGoPackage(currentFile) {
		return ""
	}
	replacer := strings.NewReplacer(".", "_", "/", "_", "-", "_")
	return replacer.Replace(goPkg)
}

func getResultType(md *descriptor.MethodDescriptorProto) string {
	return md.GetOutputType()
}

func GenerateFile(gen *protogen.Plugin, file *protogen.File, opts *types.GeneratorFuncOpts) *protogen.GeneratedFile {
	filename := file.GeneratedFilenamePrefix + "_nrpc.pb.go"
	g := gen.NewGeneratedFile(filename, file.GoImportPath)

	funcMap := template.FuncMap{
		"Prometheus": func() bool {
			return opts.Prometheus
		},
		"Tracing": func() bool {
			return opts.Tracing
		},
		"GetPackage": func() string {
			return string(file.GoPackageName)
		},
		"GetName": func(fd *descriptor.FileDescriptorProto) string {
			return strings.ReplaceAll(strings.ReplaceAll(strings.TrimSuffix(fd.GetName(), ".proto"), "-", "_"), "/", "_")
		},
		"ToTitle": strings.Title,
		"GetExtraImports": func(fd *descriptor.FileDescriptorProto) []string {
			// check all the types used and imports packages from where they come
			var imports = make(map[string]string)
			for _, sd := range fd.GetService() {
				for _, md := range sd.GetMethod() {
					goPkg, _ := getGoType(md.GetInputType(), gen.Request)
					pkgImportName := getPkgImportName(goPkg, file.Proto)
					if pkgImportName != "" {
						imports[pkgImportName] = goPkg
					}
					goPkg, _ = getGoType(getResultType(md), gen.Request)
					pkgImportName = getPkgImportName(goPkg, file.Proto)
					if pkgImportName != "" {
						imports[pkgImportName] = goPkg
					}
				}
			}
			var result []string
			for importName, goPkg := range imports {
				result = append(result,
					fmt.Sprintf("%s \"%s\"",
						importName,
						goPkg,
					),
				)
			}
			return result
		},
		"GetPkgSubject": pkgSubject,
		"GetPkgSubjectPrefix": func(fd *descriptor.FileDescriptorProto) string {
			if s := pkgSubject(fd); s != "" {
				return s + "."
			}
			return ""
		},
		"GetPkgSubjectParams": func(fd *descriptor.FileDescriptorProto) []string {
			if opts := fd.GetOptions(); opts != nil {
				e := proto.GetExtension(opts, nrpc_next.E_PackageSubjectParams)
				value := e.([]string)
				return value
			}
			return nil
		},
		"GetResultType": getResultType,
		"GetMethodSubjectParams": func(md *descriptor.MethodDescriptorProto) []string {
			if opts := md.GetOptions(); opts != nil {
				e := proto.GetExtension(opts, nrpc_next.E_MethodSubjectParams)
				if s, ok := e.([]string); ok {
					return s
				}
			}
			return []string{}
		},
		"GoType": func(pbType string) string {
			goPkg, goType := getGoType(pbType, gen.Request)
			if goPkg != "" {
				importName := getPkgImportName(goPkg, file.Proto)
				if importName != "" {
					goType = importName + "." + goType
				}
			}
			return goType
		},
		"HasStreamedReply": func(md *descriptor.MethodDescriptorProto) bool {
			if opts := md.GetOptions(); opts != nil {
				e := proto.GetExtension(opts, nrpc_next.E_StreamedReply)
				if s, ok := e.(bool); ok {
					return s
				}
			}
			return false
		},
		"GetServiceSubject": func(sd *descriptor.ServiceDescriptorProto) string {
			if opts := sd.GetOptions(); opts != nil {
				s := proto.GetExtension(opts, nrpc_next.E_ServiceSubject)
				if value, ok := s.(string); ok && s != "" {
					return value
				}
			}
			if opts := file.Proto.GetOptions(); opts != nil {
				s := proto.GetExtension(opts, nrpc_next.E_ServiceSubjectRule)
				switch s.(nrpc_next.SubjectRule) {
				case nrpc_next.SubjectRule_COPY:
					return sd.GetName()
				case nrpc_next.SubjectRule_TOLOWER:
					return strings.ToLower(sd.GetName())
				}
			}
			return sd.GetName()
		},
		"GetServiceSubjectParams": func(sd *descriptor.ServiceDescriptorProto) []string {
			if opts := sd.GetOptions(); opts != nil {
				e := proto.GetExtension(opts, nrpc_next.E_ServiceSubjectParams)
				if s, ok := e.([]string); ok {
					return s
				}
			}
			return []string{}
		},
		"GetMethodSubject": func(md *descriptor.MethodDescriptorProto) string {
			if opts := md.GetOptions(); opts != nil {
				s := proto.GetExtension(opts, nrpc_next.E_MethodSubject)
				if value, ok := s.(string); ok && value != "" {
					return value
				}
			}
			if opts := file.Proto.GetOptions(); opts != nil {
				s := proto.GetExtension(opts, nrpc_next.E_MethodSubjectRule)
				switch s.(nrpc_next.SubjectRule) {
				case nrpc_next.SubjectRule_COPY:
					return md.GetName()
				case nrpc_next.SubjectRule_TOLOWER:
					return strings.ToLower(md.GetName())
				}
			}
			return md.GetName()
		},
		"ServiceNeedsHandler": func(sd *descriptor.ServiceDescriptorProto) bool {
			for _, md := range sd.GetMethod() {
				if md.GetInputType() != ".nrpc.NoRequest" {
					return true
				}
			}
			return false
		},
	}

	t := template.Must(template.New(".").Funcs(funcMap).Parse(tFile))
	err := t.Execute(g, file.Proto)
	if err != nil {
		panic(err)
	}

	return g
}
