package types

import "google.golang.org/protobuf/compiler/protogen"

type GeneratorFuncOpts struct {
	Prometheus bool
	Tracing    bool
}
type GeneratorFuncType func(gen *protogen.Plugin, file *protogen.File, opts *GeneratorFuncOpts) *protogen.GeneratedFile
