module gitlab.com/TECHNOFAB/nrpc-next/protoc-gen-nrpc-next

go 1.18

require (
	github.com/golang/protobuf v1.5.2
	gitlab.com/TECHNOFAB/nrpc-next/nrpc.go v0.0.0-20221031175155-4f0dde604a8f
	google.golang.org/protobuf v1.28.1
)

require (
	github.com/go-logr/logr v1.2.3 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/nats-io/nats.go v1.19.0 // indirect
	github.com/nats-io/nkeys v0.3.0 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	go.opentelemetry.io/otel v1.11.1 // indirect
	go.opentelemetry.io/otel/trace v1.11.1 // indirect
	golang.org/x/crypto v0.1.0 // indirect
)
