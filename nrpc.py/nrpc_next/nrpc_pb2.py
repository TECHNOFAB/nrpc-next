# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: nrpc.proto
"""Generated protocol buffer code."""
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from google.protobuf import descriptor_pb2 as google_dot_protobuf_dot_descriptor__pb2


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\nnrpc.proto\x12\tnrpc_next\x1a google/protobuf/descriptor.proto\"\x8b\x01\n\x05\x45rror\x12#\n\x04type\x18\x01 \x01(\x0e\x32\x15.nrpc_next.Error.Type\x12\x0f\n\x07message\x18\x02 \x01(\t\x12\x10\n\x08msgCount\x18\x03 \x01(\r\":\n\x04Type\x12\n\n\x06\x43LIENT\x10\x00\x12\n\n\x06SERVER\x10\x01\x12\x07\n\x03\x45OS\x10\x03\x12\x11\n\rSERVERTOOBUSY\x10\x04\"\x06\n\x04Void\"\x0b\n\tNoRequest\"\t\n\x07NoReply\"\x1d\n\tHeartBeat\x12\x10\n\x08lastbeat\x18\x01 \x01(\x08*$\n\x0bSubjectRule\x12\x08\n\x04\x43OPY\x10\x00\x12\x0b\n\x07TOLOWER\x10\x01:6\n\x0epackageSubject\x12\x1c.google.protobuf.FileOptions\x18\xcc\xb1\x03 \x01(\t:<\n\x14packageSubjectParams\x12\x1c.google.protobuf.FileOptions\x18\xcd\xb1\x03 \x03(\t:R\n\x12serviceSubjectRule\x12\x1c.google.protobuf.FileOptions\x18\xce\xb1\x03 \x01(\x0e\x32\x16.nrpc_next.SubjectRule:Q\n\x11methodSubjectRule\x12\x1c.google.protobuf.FileOptions\x18\xcf\xb1\x03 \x01(\x0e\x32\x16.nrpc_next.SubjectRule:9\n\x0eserviceSubject\x12\x1f.google.protobuf.ServiceOptions\x18\xb4\xb9\x03 \x01(\t:?\n\x14serviceSubjectParams\x12\x1f.google.protobuf.ServiceOptions\x18\xb5\xb9\x03 \x03(\t:7\n\rmethodSubject\x12\x1e.google.protobuf.MethodOptions\x18\x9c\xc1\x03 \x01(\t:=\n\x13methodSubjectParams\x12\x1e.google.protobuf.MethodOptions\x18\x9d\xc1\x03 \x03(\t:7\n\rstreamedReply\x12\x1e.google.protobuf.MethodOptions\x18\x9e\xc1\x03 \x01(\x08\x42 Z\x1egitlab.com/TECHNOFAB/nrpc-nextb\x06proto3')

_SUBJECTRULE = DESCRIPTOR.enum_types_by_name['SubjectRule']
SubjectRule = enum_type_wrapper.EnumTypeWrapper(_SUBJECTRULE)
COPY = 0
TOLOWER = 1

PACKAGESUBJECT_FIELD_NUMBER = 55500
packageSubject = DESCRIPTOR.extensions_by_name['packageSubject']
PACKAGESUBJECTPARAMS_FIELD_NUMBER = 55501
packageSubjectParams = DESCRIPTOR.extensions_by_name['packageSubjectParams']
SERVICESUBJECTRULE_FIELD_NUMBER = 55502
serviceSubjectRule = DESCRIPTOR.extensions_by_name['serviceSubjectRule']
METHODSUBJECTRULE_FIELD_NUMBER = 55503
methodSubjectRule = DESCRIPTOR.extensions_by_name['methodSubjectRule']
SERVICESUBJECT_FIELD_NUMBER = 56500
serviceSubject = DESCRIPTOR.extensions_by_name['serviceSubject']
SERVICESUBJECTPARAMS_FIELD_NUMBER = 56501
serviceSubjectParams = DESCRIPTOR.extensions_by_name['serviceSubjectParams']
METHODSUBJECT_FIELD_NUMBER = 57500
methodSubject = DESCRIPTOR.extensions_by_name['methodSubject']
METHODSUBJECTPARAMS_FIELD_NUMBER = 57501
methodSubjectParams = DESCRIPTOR.extensions_by_name['methodSubjectParams']
STREAMEDREPLY_FIELD_NUMBER = 57502
streamedReply = DESCRIPTOR.extensions_by_name['streamedReply']

_ERROR = DESCRIPTOR.message_types_by_name['Error']
_VOID = DESCRIPTOR.message_types_by_name['Void']
_NOREQUEST = DESCRIPTOR.message_types_by_name['NoRequest']
_NOREPLY = DESCRIPTOR.message_types_by_name['NoReply']
_HEARTBEAT = DESCRIPTOR.message_types_by_name['HeartBeat']
_ERROR_TYPE = _ERROR.enum_types_by_name['Type']
Error = _reflection.GeneratedProtocolMessageType('Error', (_message.Message,), {
  'DESCRIPTOR' : _ERROR,
  '__module__' : 'nrpc_pb2'
  # @@protoc_insertion_point(class_scope:nrpc_next.Error)
  })
_sym_db.RegisterMessage(Error)

Void = _reflection.GeneratedProtocolMessageType('Void', (_message.Message,), {
  'DESCRIPTOR' : _VOID,
  '__module__' : 'nrpc_pb2'
  # @@protoc_insertion_point(class_scope:nrpc_next.Void)
  })
_sym_db.RegisterMessage(Void)

NoRequest = _reflection.GeneratedProtocolMessageType('NoRequest', (_message.Message,), {
  'DESCRIPTOR' : _NOREQUEST,
  '__module__' : 'nrpc_pb2'
  # @@protoc_insertion_point(class_scope:nrpc_next.NoRequest)
  })
_sym_db.RegisterMessage(NoRequest)

NoReply = _reflection.GeneratedProtocolMessageType('NoReply', (_message.Message,), {
  'DESCRIPTOR' : _NOREPLY,
  '__module__' : 'nrpc_pb2'
  # @@protoc_insertion_point(class_scope:nrpc_next.NoReply)
  })
_sym_db.RegisterMessage(NoReply)

HeartBeat = _reflection.GeneratedProtocolMessageType('HeartBeat', (_message.Message,), {
  'DESCRIPTOR' : _HEARTBEAT,
  '__module__' : 'nrpc_pb2'
  # @@protoc_insertion_point(class_scope:nrpc_next.HeartBeat)
  })
_sym_db.RegisterMessage(HeartBeat)

if _descriptor._USE_C_DESCRIPTORS == False:
  google_dot_protobuf_dot_descriptor__pb2.FileOptions.RegisterExtension(packageSubject)
  google_dot_protobuf_dot_descriptor__pb2.FileOptions.RegisterExtension(packageSubjectParams)
  google_dot_protobuf_dot_descriptor__pb2.FileOptions.RegisterExtension(serviceSubjectRule)
  google_dot_protobuf_dot_descriptor__pb2.FileOptions.RegisterExtension(methodSubjectRule)
  google_dot_protobuf_dot_descriptor__pb2.ServiceOptions.RegisterExtension(serviceSubject)
  google_dot_protobuf_dot_descriptor__pb2.ServiceOptions.RegisterExtension(serviceSubjectParams)
  google_dot_protobuf_dot_descriptor__pb2.MethodOptions.RegisterExtension(methodSubject)
  google_dot_protobuf_dot_descriptor__pb2.MethodOptions.RegisterExtension(methodSubjectParams)
  google_dot_protobuf_dot_descriptor__pb2.MethodOptions.RegisterExtension(streamedReply)

  DESCRIPTOR._options = None
  DESCRIPTOR._serialized_options = b'Z\036gitlab.com/TECHNOFAB/nrpc-next'
  _SUBJECTRULE._serialized_start=264
  _SUBJECTRULE._serialized_end=300
  _ERROR._serialized_start=60
  _ERROR._serialized_end=199
  _ERROR_TYPE._serialized_start=141
  _ERROR_TYPE._serialized_end=199
  _VOID._serialized_start=201
  _VOID._serialized_end=207
  _NOREQUEST._serialized_start=209
  _NOREQUEST._serialized_end=220
  _NOREPLY._serialized_start=222
  _NOREPLY._serialized_end=231
  _HEARTBEAT._serialized_start=233
  _HEARTBEAT._serialized_end=262
# @@protoc_insertion_point(module_scope)
