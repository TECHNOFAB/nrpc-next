#!/usr/bin/python3
# -*- coding: utf-8 -*-
import re
import setuptools

version = ''
with open('nrpc_next/__init__.py') as f:
    version = re.search(r'^__version__\s*=\s*[\'"]([^\'"]*)[\'"]', f.read(), re.MULTILINE).group(1)

if not version:
    raise RuntimeError('Version is not set!')

setuptools.setup(
    name='nrpc_next',
    version=version,
    description='NRPC for Python',
    author='Technofab',
    packages=setuptools.find_packages(),
    install_requires=[
        'nats-py~=2.1.3',
        'protobuf~=4.21.2'
    ],
    python_requires='>=3.6',
)
