# NRPC-next
Tools to use NATS and Protobuf to communicate between services. \
This combines https://github.com/nats-rpc/nrpc and https://github.com/nats-rpc/python-nrpc 
with added extra features (see below).

# Features
- Prometheus metrics (Go only)
- [Load balancing](https://github.com/nats-rpc/nrpc/wiki/Load-Balancing)
- easier Service Discovery, services just need to know the address of the NATS server 

# Extra Features
- Tracing (Go only)
- generate Go and Python code (and easily extendable)
- less compatibility issues with other protobuf libraries because of duplicate option keys 
